=============
Testing Fixml
=============

Running unit tests
==================
To run unit tests execute:

.. sourcecode:: bash

    $ ./test_robotfixml.py

Test structure
==============
The XML data for tests is in *data/* directory. *orig_output.xml* has been
generated from the Robot Framework test suite *data/suite* by running it with
Pybot.

All the other XML files in *data/* are broken. They have been generated from
*orig_output.xml* by removing different parts of the markup by hand.

The unit tests run the broken XML files through Fixml and write the processed
files in *output/* folder. The processed files are then parsed back to Robot
Framework result objects to verify that they are indeed repaired.
