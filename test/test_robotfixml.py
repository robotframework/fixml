#!/usr/bin/env python

"""Unit tests for robotfixml tool

Robot Framework must be in PYTHONPATH to satisfy dependencies for the tests.
See installation instructions for Robot Framework in
http://www.robotframework.org
"""

import os
import sys
import unittest
from shutil import copy2
from robot.result import ExecutionResult
from robot.utils.asserts import assert_equals

BASEDIR = os.path.dirname(os.path.abspath(__file__))
# Append source to PYTHONPATH
sys.path.append(os.path.join(BASEDIR, '..', 'src'))

import robotfixml

DATADIR = os.path.join(BASEDIR, 'data')
OUTDIR = os.path.join(BASEDIR, 'output')
if not os.path.exists(OUTDIR):
    os.mkdir(OUTDIR)


class TestRobotfixml(unittest.TestCase):

    def test_missing_kw_in_passing_test(self):
        suite = self._fix_xml_and_parse('passing_kw_missing_end_tag')
        assert_equals(len(suite.tests), 2)
        self._assert_statistics(suite, 1, 1)
        assert_equals(len(suite.tests[0].keywords[0].keywords), 2)

    def test_missing_kw_in_failing_test(self):
        suite = self._fix_xml_and_parse('failing_kw_missing_end_tag')
        assert_equals(len(suite.tests), 2)
        self._assert_statistics(suite, 1, 1)
        assert_equals(len(suite.tests[1].keywords[0].keywords), 1)

    def test_xml_cut_inside_keyword(self):
        suite = self._fix_xml_and_parse('cut_inside_kw')
        assert_equals(len(suite.tests), 1)
        self._assert_statistics(suite, 0, 1)
        assert_equals(len(suite.tests[0].keywords[0].keywords), 2)

    def test_xml_cut_inside_msg_tag(self):
        suite = self._fix_xml_and_parse('cut_inside_msg')
        assert_equals(len(suite.tests), 2)
        self._assert_statistics(suite, 1, 1)
        assert_equals(len(suite.tests[1].keywords[0].keywords), 1)

    def test_with_increasing_cut_from_end(self):
        input = open(os.path.join(DATADIR, 'orig-output.xml')).read()
        while len(input) > 250:
            input = input[:-10]
            fixxxed = robotfixml.Fixxxer(input)
            suite = ExecutionResult(str(fixxxed)).suite
            assert_equals(suite.name, 'Suite')

    def test_unicode_xml(self):
        suite = self._fix_xml_and_parse('unicode_cut_inside_kw')
        assert_equals(len(suite.tests), 1)
        self._assert_statistics(suite, 0, 1)
        assert_equals(len(suite.tests[0].keywords[0].keywords), 2)
        assert_equals(suite.name,
                      u'Unicode suite name \u2603\u00E4\u00F6')
        assert_equals(suite.tests[0].name,
                      u'Unicode test name \u2603\u00E4\u00F6')
        assert_equals(suite.tests[0].keywords[0].name,
                      u'Unicode keyword name \u2603\u00E4\u00F6')
        assert_equals(suite.tests[0].keywords[0].keywords[1].messages[0]
                      .message, u'Unicode msg \u2603\u00E4\u00F6')

    def test_fix_in_place(self):
        testfile = os.path.join(OUTDIR, 'inplace.xml')
        copy2(os.path.join(DATADIR, 'cut_inside_kw.xml'), testfile)
        robotfixml.fixml(testfile, testfile)
        suite = ExecutionResult(testfile).suite.suites[0]
        assert_equals(len(suite.tests), 1)
        self._assert_statistics(suite, 0, 1)
        assert_equals(len(suite.tests[0].keywords[0].keywords), 2)

    def _fix_xml_and_parse(self, base):
        infile = os.path.join(DATADIR, '%s.xml' % base)
        outfile = os.path.join(OUTDIR, '%s-fixed.xml' % base)
        robotfixml.fixml(infile, outfile)
        return ExecutionResult(outfile).suite.suites[0]

    def _assert_statistics(self, suite, passed, failed):
        assert_equals(suite.statistics.critical.passed, passed)
        assert_equals(suite.statistics.critical.failed, failed)


if __name__ == '__main__':
    unittest.main()
